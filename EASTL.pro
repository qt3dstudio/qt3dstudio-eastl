TARGET = EASTL
include($$PWD/../../../commonplatform.pri)
CONFIG += staticlib

*-icc*: {
QMAKE_CFLAGS += -fasm-blocks
QMAKE_CXXFLAGS += -fasm-blocks
QMAKE_CXXFLAGS_WARN_ON = -wd1418,2259,383,981,2259,11074,11076
QMAKE_CFLAGS_WARN_ON = -wd1418,2259,383,981,2259,11074,11076
}

INCLUDEPATH += UnknownVersion/include

include(EASTL.pri)

load(qt_helper_lib)
